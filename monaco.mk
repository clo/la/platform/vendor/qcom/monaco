
BUILD_BROKEN_DUP_RULES := true

########## WEARABLES Specific Flags  #####################

TARGET_SUPPORTS_WEARABLES := true

TARGET_BOARD_PLATFORM := monaco
QC_PROP_ROOT := vendor/qcom/proprietary
QC_PATH := vendor/qcom/proprietary
# Flag to be used when applicable only for LW
TARGET_SUPPORTS_WEAR_OS := true
# todo: TARGET_SUPPORTS_ANDROID_WEAR to be removed
TARGET_SUPPORTS_ANDROID_WEAR := true
TARGET_SUPPORTS_WEAR_AON := true
AUDIO_FEATURE_ENABLED_SPLIT_A2DP := true
TARGET_SYSTEM_PROP := device/qcom/monaco/system.prop
# Target supports smartPA
WEARABLE_SUPPORTS_TFA_SMARTPA := true

BOARD_COMMON_DIR := device/qcom/common
BOARD_SEPOLICY_DIR := device/qcom/sepolicy
BOARD_OPENSOURCE_DIR := vendor/qcom/opensource
BOARD_BT_DIR := hardware/qcom/bt
BOARD_WLAN_DIR := device/qcom/wlan
BOARD_DLKM_DIR := $(BOARD_COMMON_DIR)/dlkm
BOARD_DISPLAY_HAL := hardware/qcom/display
BOARD_VCOMMON_DIR := device/qcom/vendor-common

TARGET_EXCLUDES_DISPLAY_PP := true
TARGET_EXCLUDES_MULTI_DISPLAY := true

DEVICE_PACKAGE_OVERLAYS += device/qcom/monaco/overlay

CLOCKWORK_DISABLE_GOOGLE_TTS := true
INCLUDE_GOOGLE_SANS_FONT := false
CLOCKWORK_ENABLE_HERO_ANGELSWORD_WATCHFACES :=
CLOCKWORK_DISABLE_HANDWRITING := true
CLOCKWORK_INCLUDE_OEM_SETUP :=
CLOCKWORK_ENABLE_DEFAULT_CHARGING_SCREEN := true

# Add Elements watch faces if not disabled
CLOCKWORK_DISABLE_ELEMENTS_WATCHFACES := false
CLOCKWORK_DISABLE_RETAIL_ATTRACT_LOOP := false
CLOCKWORK_ENABLE_SIDEKICK_WATCHFACES := true

# Setting this flag to 'true' adds all telephony related packages listed
# in 'device/google/clockwork/build/clockwork.mk' to PRODUCT_PACKAGES
CLOCKWORK_ENABLE_TELEPHONY := true

# Use 1P sysui
CLOCKWORK_SYSUI_MODULE := ClockworkSysUiGoogle

# Enable Traditional Watch Mode (TWM)
#CLOCKWORK_ENABLE_TRADITIONAL_WATCH_MODE := true
CLOCKWORK_HOME_PREBUILTS_CUSTOM_DIR := experimental
#For targets which donot support vulkan
TARGET_NOT_SUPPORT_VULKAN := false

# Flag to be used to enable AONUtility hal
TARGET_SUPPORTS_AON_UTILITY := true

# Flag to be used to enable sidekick metrics feature
TARGET_SUPPORTS_SIDEKICK_METRICS := false

# Flag to be used to enable Wear Health Services feature
TARGET_SUPPORTS_WEAR_HEALTH_SERVICE := true

# Flag to enable DisplayOffload hal
TARGET_SUPPORTS_DISPLAY_OFFLOAD := true

# Flag to be used to enable WristOrientation hal
TARGET_SUPPORTS_WRIST_ORIENTATION := true
# Enable wrist orientation
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    config.enable_wristorientation=1

# Flag to enable DeepSleep
TARGET_SUPPORTS_DS := true

# Flag to enable Hibernate
TARGET_SUPPORTS_S2D := true

# Flag to enable Hibernation restore from ABL
TARGET_HIBERNATION_INSECURE_ENABLE := true

#Flag to enable arm mode switch in ABL Hibernation restore path
TARGET_HIBERNATION_32BIT_MODE_SWITCH := true

# Enable incremental fs
PRODUCT_PROPERTY_OVERRIDES += \
    ro.incremental.enable=yes

# Support for new wear boot animation
PRODUCT_COPY_FILES += \
     device/google/clockwork/bootanimations/square_360/bootanimation.zip:system/media/bootanimation.zip \
     $(BOARD_COMMON_DIR)/ssohal/metric_sensors_list.txt:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/metric_sensors_list.txt

# Set logcat buffer size to 1Mb for userdebug and eng build
ifneq (,$(filter eng userdebug, $(TARGET_BUILD_VARIANT)))
PRODUCT_PROPERTY_OVERRIDES += \
     ro.logd.size=1048576
endif

PRODUCT_PROPERTY_OVERRIDES += \
  dalvik.vm.dex2oat-threads=2 \
  dalvik.vm.boot-dex2oat-threads=4 \

$(call inherit-product-if-exists, vendor/qcom/gpu/monaco/monaco-gpu-vendor.mk)
############### End Wearables Specfic Flags ################

##############################Go configs###########################################
#Go variant flag
TARGET_HAS_LOW_RAM := true

#target specific runtime prop for qspm
PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.qspm.enable=true

# Reduces GC frequency of foreground apps by 50%
PRODUCT_PROPERTY_OVERRIDES += dalvik.vm.foreground-heap-growth-multiplier=2.0
# Disable per_app memcg
PRODUCT_PROPERTY_OVERRIDES += ro.config.per_app_memcg=false

PRODUCT_PACKAGES += \
    android.hardware.cas@1.2-service-lazy

PRODUCT_PACKAGES += \
    android.hardware.sensors@2.1-service.multihal \
    android.hardware.sensors@2.0-ScopedWakelock

PRODUCT_PROPERTY_OVERRIDES += persist.vendor.sensors.enable.bypass_worker=true
#Enable WearQSTA
PRODUCT_PROPERTY_OVERRIDES += ro.vendor.sensors.wearqstp=1
#WearQSTA Wakelock by default disabled
PRODUCT_PROPERTY_OVERRIDES += ro.vendor.sensors.wearqstp.lock=0

# Configstore is disabled for Android Go targets
PRODUCT_PACKAGES += disable_configstore


#########################End of Go configs########################################


# For QSSI builds, we should skip building the system image. Instead we build the
# "non-system" images (that we support).
$(call inherit-product, $(BOARD_COMMON_DIR)/common.mk)

# Temporary bring-up config -->
ALLOW_MISSING_DEPENDENCIES := true
# Enable AVB 2.0
BOARD_AVB_ENABLE := true

# Default A/B configuration
ENABLE_AB := false

# Enable virtual-ab by default
ENABLE_VIRTUAL_AB := false
$(warning " TAPAS ENABLE_VIRTUAL_AB = $(ENABLE_VIRTUAL_AB) is for Monaco")

ifeq ($(ENABLE_VIRTUAL_AB), true)
$(call inherit-product, $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)
endif

$(call inherit-product, $(SRC_TARGET_DIR)/product/emulated_storage.mk)

TARGET_USES_NQ_NFC := false
TARGET_USES_AOSP_NFC := true

# Enable Dynamic partition
BOARD_DYNAMIC_PARTITION_ENABLE ?= true

# Set API Level for R
SHIPPING_API_LEVEL ?= 30
PRODUCT_SHIPPING_API_LEVEL := $(SHIPPING_API_LEVEL)




#Suppot to compile recovery without msm headers
TARGET_HAS_GENERIC_KERNEL_HEADERS := true

#Include mainline components
PRODUCT_ENFORCE_ARTIFACT_PATH_REQUIREMENTS := true

#Enable product partition Native I/F. It is automatically set to current if
#the shipping API level for the target is greater than 29
PRODUCT_PRODUCT_VNDK_VERSION := current

#Enable product partition Java I/F. It is automatically set to true if
#the shipping API level for the target is greater than 29
PRODUCT_ENFORCE_PRODUCT_PARTITION_INTERFACE := true

ifeq ($(ENABLE_AB), true)
PRODUCT_BUILD_CACHE_IMAGE := false

else
PRODUCT_BUILD_CACHE_IMAGE := true
endif
PRODUCT_BUILD_RAMDISK_IMAGE := true
PRODUCT_BUILD_USERDATA_IMAGE := true

ifeq ($(strip $(BOARD_DYNAMIC_PARTITION_ENABLE)),true)
PRODUCT_USE_DYNAMIC_PARTITIONS := true
# Enable product partition
PRODUCT_BUILD_PRODUCT_IMAGE := true
# Enable System_ext
PRODUCT_BUILD_SYSTEM_EXT_IMAGE := true


PRODUCT_PACKAGES += fastbootd
# Add default implementation of fastboot HAL.
PRODUCT_PACKAGES += android.hardware.fastboot@1.0-impl-mock

# enable vbmeta_system
BOARD_AVB_VBMETA_SYSTEM := system product system_ext
BOARD_AVB_VBMETA_SYSTEM_KEY_PATH := external/avb/test/data/testkey_rsa2048.pem
BOARD_AVB_VBMETA_SYSTEM_ALGORITHM := SHA256_RSA2048
BOARD_AVB_VBMETA_SYSTEM_ROLLBACK_INDEX := $(PLATFORM_SECURITY_PATCH_TIMESTAMP)
BOARD_AVB_VBMETA_SYSTEM_ROLLBACK_INDEX_LOCATION := 2
$(call inherit-product, build/make/target/product/gsi_keys.mk)
endif
# f2fs utilities
PRODUCT_PACKAGES += \
 sg_write_buffer \
 f2fs_io \
 check_f2fs

# Userdata checkpoint
PRODUCT_PACKAGES += \
 checkpoint_gc

ifeq ($(ENABLE_AB), true)
# Userdata checkpoint start
AB_OTA_POSTINSTALL_CONFIG += \
RUN_POSTINSTALL_vendor=true \
POSTINSTALL_PATH_vendor=bin/checkpoint_gc \
FILESYSTEM_TYPE_vendor=ext4 \
POSTINSTALL_OPTIONAL_vendor=true
# Userdata checkpoint end
PRODUCT_COPY_FILES += $(LOCAL_PATH)/fstab_AB_dynamic_partition.qti:$(TARGET_COPY_OUT_RAMDISK)/fstab.qcom
else
PRODUCT_COPY_FILES += $(LOCAL_PATH)/fstab_non_AB_dynamic_partition.qti:$(TARGET_COPY_OUT_RAMDISK)/fstab.qcom
endif

#fstab.qcom
PRODUCT_PACKAGES += fstab.qcom

BOARD_HAVE_BLUETOOTH := false
BOARD_HAVE_QCOM_FM := false
TARGET_DISABLE_PERF_OPTIMIATIONS := false

TARGET_ENABLE_QC_AV_ENHANCEMENTS := true

TARGET_DEFINES_DALVIK_HEAP := true

# Temporary bring-up config -->
PRODUCT_SUPPORTS_VERITY := false
# Temporary bring-up config <--
###########
PRODUCT_PROPERTY_OVERRIDES  += \
     dalvik.vm.heapstartsize=8m \
     dalvik.vm.heapsize=128m \
     dalvik.vm.heaptargetutilization=0.75 \
     dalvik.vm.heapminfree=6m \
     dalvik.vm.heapmaxfree=8m

# Setting this flag to 'true' adds all telephony related packages listed
# in 'device/google/clockwork/build/clockwork.mk' to PRODUCT_PACKAGES
CLOCKWORK_ENABLE_TELEPHONY := true

# Enable Android Pay.
CLOCKWORK_ENABLE_TAPANDPAY := true
CLOCKWORK_ENABLE_GOOGLEPARTNERSETUP := true

# Enable Clockwork Display offload service
CLOCKWORK_ENABLE_DISPLAY_OFFLOAD := true

TARGET_USES_AOSP := true
TARGET_USES_AOSP_FOR_AUDIO := true
TARGET_USES_QCOM_BSP := false
ifeq ($(TARGET_USES_AOSP),true)
TARGET_DISABLE_DASH := true
endif

# Audio configuration file
ifneq ($(BOARD_OPENSOURCE_DIR), )
   -include $(TOPDIR)$(BOARD_OPENSOURCE_DIR)/audio-hal/primary-hal/configs/monaco/monaco.mk
else
   -include $(TOPDIR)vendor/qcom/opensource/audio-hal/primary-hal/configs/monaco/monaco.mk
endif # BOARD_OPENSOURCE_DIR


$(call inherit-product, device/google/clockwork/build/clockwork_google.mk)
# List available watch ringtones in Settings->Sound->Watch ringtone option
$(call inherit-product, device/google/clockwork/build/clockwork_ringtones.mk)
# Add all supported software sudio codecs to PRODUCT_PACKAGES
$(call inherit-product, device/google/clockwork/build/clockwork_audio.mk)
# include clockwork-services, if present (for full-source PDK builds)
$(call inherit-product-if-exists,vendor/google_clockwork/products/clockwork_services.mk)

# Target naming
PRODUCT_NAME := monaco
PRODUCT_DEVICE := monaco
PRODUCT_BRAND := google
PRODUCT_MODEL := Dialga



#Binder size property
PRODUCT_PROPERTY_OVERRIDES += \
    vendor.mediacodec.binder.size=256

TARGET_DISABLE_DISPLAY := false

# Kernel configurations
ifneq ($(wildcard kernel/msm-4.19),)
    TARGET_KERNEL_VERSION := 4.19
    $(warning "Build with 4.19 kernel.")
else ifneq ($(wildcard kernel/msm-5.4),)
    TARGET_KERNEL_VERSION := 5.4
    $(warning "Build with 5.4 kernel.")
else
    $(warning "Build with unknown kernel.")
endif

ifeq ($(TARGET_KERNEL_VERSION), 5.4)
TARGET_USES_5.4_KERNEL := true
endif

#Enable llvm support for kernel
KERNEL_LLVM_SUPPORT := true

#Enable aosp-llvm support for kernel 5.4 compilation
ifeq ($(TARGET_USES_5.4_KERNEL), true)
KERNEL_SD_LLVM_SUPPORT := false
else
KERNEL_SD_LLVM_SUPPORT := true
endif

#disable dpp project
ifeq ($(TARGET_USES_5.4_KERNEL), true)
TARGET_EXCLUDES_DISPLAY_PP := true
endif

#include sensors makefiles
#$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/vendor/sns_vendor_product.mk)
#$(call inherit-product-if-exists, vendor/qcom/defs/board-defs/vendor/sns_vendor_board.mk)
#$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/vendor/sns_vendor_noship_product.mk)
#$(call inherit-product-if-exists, vendor/qcom/defs/board-defs/vendor/sns_vendor_noship_board.mk)

TARGET_ENABLE_QC_AV_ENHANCEMENTS := true

# media_profiles and media_codecs xmls for monaco
ifeq ($(TARGET_ENABLE_QC_AV_ENHANCEMENTS), true)
PRODUCT_COPY_FILES += $(BOARD_COMMON_DIR)/media/media_profiles.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_profiles_vendor.xml
endif

###########
# Target configurations

QCOM_BOARD_PLATFORMS += monaco


#Default vendor image configuration
ENABLE_VENDOR_IMAGE := true

# default is nosdcard, S/W button enabled in resource
PRODUCT_CHARACTERISTICS := nosdcard,watch

BOARD_FRP_PARTITION_NAME := frp

# Android EGL implementation
PRODUCT_PACKAGES += libGLES_android

PRODUCT_PACKAGES += fs_config_files
PRODUCT_PACKAGES += qpnp_pon.kl
PRODUCT_PACKAGES += gpio-keys.kl
PRODUCT_PACKAGES += libvolumelistener

ifeq ($(ENABLE_AB), true)
# A/B related packages
PRODUCT_PACKAGES += update_engine \
    update_engine_client \
    update_verifier \
    android.hardware.boot@1.1-impl-qti \
    android.hardware.boot@1.1-impl-qti.recovery \
    android.hardware.boot@1.1-service

PRODUCT_HOST_PACKAGES += \
    brillo_update_payload
# Boot control HAL test app
PRODUCT_PACKAGES_DEBUG += bootctl

PRODUCT_PACKAGES += \
  update_engine_sideload

endif
DEVICE_FRAMEWORK_MANIFEST_FILE := device/qcom/monaco/framework_manifest.xml


PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
      ro.bt.bdaddr_path=/mnt/vendor/persist/bdaddr.txt

TARGET_USE_WEAR_QC_BT_STACK := false

PRODUCT_PROPERTY_OVERRIDES += \
persist.vendor.qcom.bluetooth.soc=slate

# Enable A2DP offload
PRODUCT_PROPERTY_OVERRIDES += \
ro.bluetooth.a2dp_offload.supported=true

# Vendor property for enable A2DP offload
PRODUCT_PROPERTY_OVERRIDES += \
persist.bluetooth.a2dp_offload.disabled =false

# A2DP offload DSP supported encoder list
PRODUCT_PROPERTY_OVERRIDES += \
persist.bluetooth.a2dp_offload.cap=sbc

DEVICE_MANIFEST_FILE := device/qcom/monaco/manifest.xml

ifneq ($(BOARD_OPENSOURCE_DIR), )
DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
    $(BOARD_COMMON_DIR)/vendor_framework_compatibility_matrix.xml \
    $(BOARD_OPENSOURCE_DIR)/core-utils/vendor_framework_compatibility_matrix.xml
else
    DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
    $(BOARD_COMMON_DIR)/vendor_framework_compatibility_matrix.xml \
    vendor/qcom/opensource/core-utils/vendor_framework_compatibility_matrix.xml
endif # BOARD_OPENSOURCE_DIR

#DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE += \
    device/qcom/monaco/vendor_framework_compatibility_matrix.xml
ifeq ($(ENABLE_AB), true)
DEVICE_MANIFEST_FILE += device/qcom/monaco/manifest_ab.xml
endif
DEVICE_MATRIX_FILE   := $(BOARD_COMMON_DIR)/compatibility_matrix.xml

# Kernel modules install path
KERNEL_MODULES_INSTALL := dlkm
KERNEL_MODULES_OUT := out/target/product/$(PRODUCT_NAME)/$(KERNEL_MODULES_INSTALL)/lib/modules

# MIDI feature
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.midi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.midi.xml

# Wearcore feature
PRODUCT_COPY_FILES += \
        frameworks/native/data/etc/wearable_core_hardware.xml:system/etc/permissions/wearable_core_hardware.xml \
        frameworks/native/data/etc/android.hardware.audio.output.xml:system/etc/permissions/android.hardware.audio.output.xml \
        frameworks/av/media/libstagefright/data/media_codecs_google_video_le.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_video_le.xml \
        frameworks/native/data/etc/android.hardware.screen.portrait.xml:system/etc/permissions/android.hardware.screen.portrait.xml \
        frameworks/native/data/etc/android.hardware.sensor.barometer.xml:system/etc/permissions/android.hardware.sensor.barometer.xml

# VB xml
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.verified_boot.xml:system/etc/permissions/android.software.verified_boot.xml

$(call inherit-product, device/google/clockwork/build/wearable-mdpi-512-dalvik-heap.mk)

#
# system prop for opengles version
#
# 196608 is decimal for 0x30000 to report version 3
# 196609 is decimal for 0x30001 to report version 3.1
# 196610 is decimal for 0x30002 to report version 3.2
PRODUCT_PROPERTY_OVERRIDES  += \
    ro.opengles.version=196609



# MIDI feature
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.midi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.midi.xml

#Enable full treble flag
PRODUCT_FULL_TREBLE_OVERRIDE := true
PRODUCT_VENDOR_MOVE_ENABLED := true
PRODUCT_COMPATIBLE_PROPERTY_OVERRIDE := true
TARGET_SUPPORT_SOTER := true
TARGET_MOUNT_POINTS_SYMLINKS := false

#set KMGK_USE_QTI_SERVICE to true to enable QTI KEYMASTER and GATEKEEPER HIDLs
ifeq ($(ENABLE_VENDOR_IMAGE), true)
KMGK_USE_QTI_SERVICE := true
endif

BOARD_VNDK_VERSION := current
TARGET_MOUNT_POINTS_SYMLINKS := false

PRODUCT_BOOT_JARS += telephony-ext
PRODUCT_PACKAGES += telephony-ext

# Enable telephpony ims feature
PRODUCT_COPY_FILES += \
     frameworks/native/data/etc/android.hardware.telephony.ims.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.telephony.ims.xml

PRODUCT_BOOT_JARS += tcmiface

#Charger
ifeq ($(ENABLE_AB),true)
   PRODUCT_COPY_FILES += \
        device/qcom/monaco/qbg_coproc_fstab.qti:$(TARGET_COPY_OUT_VENDOR)/etc/qbg_coproc_fstab.qti
else
   PRODUCT_COPY_FILES += \
        device/qcom/monaco/qbg_coproc_non-AB_variant.qti:$(TARGET_COPY_OUT_VENDOR)/etc/qbg_coproc_fstab.qti
endif

PRODUCT_PACKAGES += qbg_coproc_fstab.qti

# Vendor property to enable advanced network scanning
PRODUCT_PROPERTY_OVERRIDES += \
    persist.vendor.radio.enableadvancedscan=true

# Property to disable ZSL mode
PRODUCT_PROPERTY_OVERRIDES += \
    camera.disable_zsl_mode=1

# Enable qcrild and disable rild
ENABLE_VENDOR_RIL_SERVICE := true
PRODUCT_PROPERTY_OVERRIDES += \
ro.crypto.volume.filenames_mode = "aes-256-cts" \
ro.crypto.allow_encrypt_override = true

dex2oat_blacklist := ClockworkPlayAutoInstallConfig \
                     ClockworkBugReportSender \
                     PrebuiltBugleWearable \
                     PrebuiltDeskClockMicroApp \
                     TalkbackWearPrebuilt \
                     TranslatePrebuiltWearable \
                     VZMessages
$(call add-product-dex-preopt-module-config,$(dex2oat_blacklist),disable)

PRODUCT_PACKAGES += slate-spi.idc
PRODUCT_COPY_FILES += \
		 device/qcom/monaco/slate-spi.idc:system/usr/idc/slate-spi.idc

# diag-router
TARGET_HAS_DIAG_ROUTER := true

#----------------------------------------------------------------------
# wlan specific
#----------------------------------------------------------------------
ifneq ($(BOARD_WLAN_DIR), )
   include $(BOARD_WLAN_DIR)/monaco/wlan.mk
else
   include device/qcom/wlan/monaco/wlan.mk
endif # BOARD_WLAN_DIR

###################################################################################
# This is the End of target.mk file.
# Now, Pickup other split product.mk files:
###################################################################################

$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/vendor/*.mk)
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/system/*.mk)

#include adsprpc makefiles
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/adsprpc_vendor_product.mk)


#############include BT make files#################################################
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/bt-system-opensource-product.mk)
ifeq ($(TARGET_USE_WEAR_QC_BT_STACK),true)
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/bt-system-proprietary-product.mk)
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/bt-vendor-proprietary-product.mk)
endif
###################################################################################


# include gps makefiles
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/gps-product.mk)
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/gps-system-product.mk)
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/gps-product-hal.mk)
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/gps-product-opensource.mk)
$(call inherit-product-if-exists, vendor/qcom/defs/board-defs/legacy/gps-board.mk)
$(call inherit-product-if-exists, vendor/qcom/defs/board-defs/legacy/gps-system-board.mk)

#include power makefiles
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/power-vendor-product.mk)
$(call inherit-product-if-exists, vendor/qcom/defs/board-defs/legacy/power-vendor-board.mk)

#Enable vibrator packages
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/vibrator_product.mk)

#Enable perf products if exiss
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/legacy/perf-product-*.mk)
###################################################################################

PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
ro.vendor.qc_aon_presence = 1

PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
ro.vendor.enable_qc_dsframework = 0

RIL_USE_ADB_PROP_FOR_APM_SIM_NOT_PWDN := true
PRODUCT_PROPERTY_OVERRIDES += persist.vendor.radio.apm_sim_not_pwdn=0
