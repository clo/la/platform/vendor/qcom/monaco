# config.mk
#
# Product-specific compile-time definitions.
#

BOARD_SYSTEMSDK_VERSIONS := $(SHIPPING_API_LEVEL)

TARGET_BOARD_PLATFORM := monaco
TARGET_BOOTLOADER_BOARD_NAME := monaco

TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv8-a
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_CPU_VARIANT := cortex-a73

#Generate DTBO image
BOARD_KERNEL_SEPARATED_DTBO := true

TARGET_NO_BOOTLOADER := false
TARGET_USES_UEFI := true
TARGET_NO_KERNEL := false

BOARD_PRESIL_BUILD := true

-include $(QCPATH)/common/monaco/BoardConfigVendor.mk
include device/google/clockwork/build/ClockworkBoardConfig.mk

SECTOOLS_SECURITY_PROFILE := $(QCPATH)/securemsm/security_profiles/atherton_tz_security_profile.xml
USESECTOOLV2 := true
USE_OPENGL_RENDERER := true

##############################Go configs###########################################
MALLOC_SVELTE := true
#########################End of Go configs########################################

#Enable dtb in boot image and boot image header version 2 support.
BOARD_INCLUDE_DTB_IN_BOOTIMG := true
# Set Header version for bootimage
BOARD_BOOTIMG_HEADER_VERSION := 2
BOARD_MKBOOTIMG_ARGS := --header_version $(BOARD_BOOTIMG_HEADER_VERSION)

ifeq ($(ENABLE_AB), true)
# Defines for enabling A/B builds
AB_OTA_UPDATER := true
# Full A/B partition update set
# AB_OTA_PARTITIONS := xbl rpm tz hyp pmic modem abl boot keymaster cmnlib cmnlib64 system bluetooth

# Minimum partition set for automation to test recovery generation code
# Packages generated by using just the below flag cannot be used for updating a device. You must pass
# in the full set mentioned above as part of your make commandline
AB_OTA_PARTITIONS ?= boot vendor system system_ext product vbmeta_system
else
# Non-A/B section. Define cache and recovery partition variables.
BOARD_CACHEIMAGE_PARTITION_SIZE := 115343360
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4

ifeq ($(BOARD_AVB_ENABLE), true)
   BOARD_AVB_RECOVERY_KEY_PATH := external/avb/test/data/testkey_rsa4096.pem
   BOARD_AVB_RECOVERY_ALGORITHM := SHA256_RSA4096
   BOARD_AVB_RECOVERY_ROLLBACK_INDEX := 1
   BOARD_AVB_RECOVERY_ROLLBACK_INDEX_LOCATION := 1

   BOARD_AVB_SYSTEM_ADD_HASHTREE_FOOTER_ARGS += --hash_algorithm sha256
   BOARD_AVB_VENDOR_ADD_HASHTREE_FOOTER_ARGS += --hash_algorithm sha256
   BOARD_AVB_PRODUCT_ADD_HASHTREE_FOOTER_ARGS += --hash_algorithm sha256
   BOARD_AVB_SYSTEM_EXT_ADD_HASHTREE_FOOTER_ARGS += --hash_algorithm sha256
   BOARD_AVB_SYSTEM_OTHER_ADD_HASHTREE_FOOTER_ARGS += --hash_algorithm sha256
endif
endif

BOARD_USES_METADATA_PARTITION := true

# Product partition support
  TARGET_COPY_OUT_PRODUCT := product
  BOARD_USES_PRODUCTIMAGE := true
  BOARD_PRODUCTIMAGE_FILE_SYSTEM_TYPE := ext4
  # System_ext support
  TARGET_COPY_OUT_SYSTEM_EXT := system_ext
  BOARD_SYSTEM_EXTIMAGE_FILE_SYSTEM_TYPE := ext4

### Dynamic partition Handling
# Define the Dynamic Partition sizes and groups.
     ifeq ($(ENABLE_AB), true)
      ifeq ($(ENABLE_VIRTUAL_AB), true)
        BOARD_SUPER_PARTITION_SIZE := 2147483648
        TARGET_RECOVERY_FSTAB := device/qcom/monaco/recovery_AB_dynamic_partition.fstab
       else
         BOARD_SUPER_PARTITION_SIZE := 4096000000
       endif
    else
        BOARD_SUPER_PARTITION_SIZE := 2147483648
        TARGET_RECOVERY_FSTAB := device/qcom/monaco/recovery_non-AB_dynamic_partition.fstab
    endif
    ifeq ($(BOARD_KERNEL_SEPARATED_DTBO),true)
        # Enable DTBO for recovery image
        BOARD_INCLUDE_RECOVERY_DTBO := true
    endif
    BOARD_SUPER_PARTITION_GROUPS := qti_dynamic_partitions
    BOARD_QTI_DYNAMIC_PARTITIONS_SIZE := 1979711488
    BOARD_QTI_DYNAMIC_PARTITIONS_PARTITION_LIST := vendor system product system_ext
    BOARD_EXT4_SHARE_DUP_BLOCKS := true
    BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x04400000

### Dynamic partition Handling

#Enable compilation of oem-extensions to recovery
#These need to be explicitly
ifneq ($(AB_OTA_UPDATER),true)
    TARGET_RECOVERY_UPDATER_LIBS += librecovery_updater_msm
endif

TARGET_COPY_OUT_VENDOR := vendor
BOARD_PROPERTY_OVERRIDES_SPLIT_ENABLED := true

ifneq ($(TARGET_USE_WEAR_QC_BT_STACK),true)
BOARD_HAVE_BLUETOOTH := true
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := \
   device/google/clockwork/include/bluetooth \
   $(BOARD_COMMON_DIR)
endif

TARGET_USERIMAGES_USE_EXT4 := true
TARGET_USERIMAGES_USE_F2FS := true
BOARD_USERDATAIMAGE_FILE_SYSTEM_TYPE := f2fs
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x06000000
BOARD_USERDATAIMAGE_PARTITION_SIZE := 4939212390
BOARD_PERSISTIMAGE_PARTITION_SIZE := 33554432
BOARD_PERSISTIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_METADATAIMAGE_PARTITION_SIZE := 67108864
BOARD_DTBOIMG_PARTITION_SIZE := 0x0800000
BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_FLASH_BLOCK_SIZE := 131072 # (BOARD_KERNEL_PAGESIZE * 64)

BOARD_DO_NOT_STRIP_VENDOR_MODULES := true
TARGET_USES_ION := true
TARGET_USES_NEW_ION_API := true

BOARD_KERNEL_CMDLINE := console=ttyMSM0,115200n8 earlycon=msm_geni_serial,0x4a98000 androidboot.hardware=qcom androidboot.console=ttyMSM0 androidboot.memcg=1 lpm_levels.sleep_disabled=1 video=vfb:640x400,bpp=32,memsize=3072000 msm_rtb.filter=0x237 service_locator.enable=1 swiotlb=2048 vmalloc=115M loop.max_part=7 iptable_raw.raw_before_defrag=1 ip6table_raw.raw_before_defrag=1 hibernate=nocompress nokaslr noswap_randomize cgroup.memory=nokmem,nosocket

BOARD_KERNEL_BASE        := 0x00000000
BOARD_KERNEL_PAGESIZE    := 4096
BOARD_KERNEL_TAGS_OFFSET := 0x01E00000
BOARD_RAMDISK_OFFSET     := 0x02000000

TARGET_KERNEL_ARCH := arm
TARGET_KERNEL_CROSS_COMPILE_PREFIX := $(shell pwd)/prebuilts/gcc/linux-x86/arm/arm-linux-androideabi-4.9/bin/arm-linux-androidkernel-
TARGET_USES_UNCOMPRESSED_KERNEL := false

MAX_EGL_CACHE_KEY_SIZE := 12*1024
MAX_EGL_CACHE_SIZE := 2048*1024

BOARD_USES_GENERIC_AUDIO := true
BOARD_QTI_CAMERA_32BIT_ONLY := true
TARGET_NO_RPC := true

TARGET_PLATFORM_DEVICE_BASE := /devices/soc.0/
TARGET_INIT_VENDOR_LIB := libinit_msm

#Disable appended dtb.
TARGET_KERNEL_APPEND_DTB := false
TARGET_COMPILE_WITH_MSM_KERNEL := true

#Enable PD locater/notifier
TARGET_PD_SERVICE_ENABLED := true

#Enable peripheral manager
TARGET_PER_MGR_ENABLED := true

# Enable dex pre-opt to speed up initial boot
ifeq ($(HOST_OS),linux)
    ifeq ($(WITH_DEXPREOPT),)
    WITH_DEXPREOPT := true
    WITH_DEXPREOPT_PIC := true
    ifneq ($(TARGET_BUILD_VARIANT),user)
           # Retain classes.dex in APK's for non-user builds
            DEX_PREOPT_DEFAULT := nostripping
        endif
    endif
endif

#Add non-hlos files to ota packages
ADD_RADIO_FILES := true

# Enable sensor multi HAL
USE_SENSOR_MULTI_HAL := true

#namespace definition for librecovery_updater
#differentiate legacy 'sg' or 'bsg' 'non-ufs' framework
SOONG_CONFIG_NAMESPACES += ufsbsg

SOONG_CONFIG_ufsbsg += ufsframework
SOONG_CONFIG_ufsbsg_ufsframework := non_ufs

ifneq (,$(filter userdebug eng, $(TARGET_BUILD_VARIANT)))
    ifeq (,$(findstring perf_defconfig, $(KERNEL_DEFCONFIG)))
        ifneq ($(TARGET_USES_5.4_KERNEL), true)
        BOARD_VENDOR_KERNEL_MODULES += $(KERNEL_MODULES_OUT)/lkdtm.ko
        endif
    endif
endif

#################################################################################
# This is the End of BoardConfig.mk file.
# Now, Pickup other split Board.mk files:
#################################################################################
-include vendor/qcom/defs/board-defs/system/*.mk
-include vendor/qcom/defs/board-defs/vendor/*.mk
ifeq ($(TARGET_USE_WEAR_QC_BT_STACK),true)
-include vendor/qcom/defs/board-defs/legacy/bt-commonsys-intf-legacy-board.mk
-include vendor/qcom/defs/board-defs/legacy/bt-vendor-proprietary-board.mk
endif

BUILD_BROKEN_DUP_RULES := true
TARGET_USES_64_BIT_BINDER := true

# Enable QG user space
PMIC_QG_SUPPORT := true

HAPTICS_OFFLOAD_SUPPORT := true

#flag for qspm compilation
TARGET_USES_QSPM := true

#----------------------------------------------------------------------
# wlan specific
#----------------------------------------------------------------------
ifeq ($(strip $(BOARD_HAS_QCOM_WLAN)),true)
ifneq ($(BOARD_WLAN_DIR), )
   include $(BOARD_WLAN_DIR)/monaco/BoardConfigWlan.mk
else
   include device/qcom/wlan/monaco/BoardConfigWlan.mk
endif # BOARD_WLAN_DIR
endif # BOARD_HAS_QCOM_WLAN

BUILD_BROKEN_NINJA_USES_ENV_VARS := SDCLANG_AE_CONFIG SDCLANG_CONFIG SDCLANG_SA_ENABLED
BUILD_BROKEN_NINJA_USES_ENV_VARS += TEMPORARY_DISABLE_PATH_RESTRICTIONS
BUILD_BROKEN_NINJA_USES_ENV_VARS += RTIC_MPGEN
BUILD_BROKEN_PREBUILT_ELF_FILES := true
BUILD_BROKEN_USES_BUILD_HOST_SHARED_LIBRARY := true
BUILD_BROKEN_USES_BUILD_HOST_STATIC_LIBRARY := true
BUILD_BROKEN_USES_BUILD_HOST_EXECUTABLE := true
BUILD_BROKEN_USES_BUILD_COPY_HEADERS := true

ifneq ($(BOARD_SEPOLICY_DIR), )
   include $(BOARD_SEPOLICY_DIR)/SEPolicy.mk
else
   include device/qcom/sepolicy/SEPolicy.mk
endif # BOARD_SEPOLICY_DIR

